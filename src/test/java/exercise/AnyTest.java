package exercise;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;


public class AnyTest {

    private static final String USER_FILE_PATH = "src/main/resources/users.csv";

    private static final String PURCHASE_FILE_PATH = "src/main/resources/purchases.csv";


    private Table createTable(String filePath) {
        File userFile = new File(filePath);
        Sql sql = new Sql();
        Table table;
        try {
            table = sql.init(new FileInputStream(userFile));
            return table;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Test
    public void testInitUserTable() {
        Table userTable = createTable(USER_FILE_PATH);
        // Check the size of the content
        Assert.assertEquals(userTable.getRowCount(), 5);

        // check the values for the first row
        Row row = userTable.getRow(0);
        Assert.assertEquals("2", row.getValue("USER_ID"));
        Assert.assertEquals("manuel", row.getValue("NAME"));
        Assert.assertEquals("manuel@foo.de", row.getValue("EMAIL"));

    }

    @Test
    public void testPurchaseUserTable() {
        Table purchaseTable = createTable(PURCHASE_FILE_PATH);

        // Check the size of the content
        Assert.assertEquals(purchaseTable.getRowCount(), 8);

        // check the values for the first row
        Row row = purchaseTable.getRow(0);
        Assert.assertEquals("1", row.getValue("USER_ID"));
        Assert.assertEquals("1", row.getValue("AD_ID"));
        Assert.assertEquals("car-1", row.getValue("TITLE"));
    }

    @Test
    public void testOrderByID() {
        Table userTable = createTable(USER_FILE_PATH);
        Sql sql = new Sql();
        Table sortedTable = sql.orderByDesc(userTable, "USER_ID");

        Row row = sortedTable.getRow(0);
        Assert.assertEquals("paul", row.getValue("NAME"));
    }

    @Test
    public void testSuccessJoin() {
        Sql sql = new Sql();

        Table userTable = createTable(USER_FILE_PATH);
        Table purchaseTable = createTable(PURCHASE_FILE_PATH);

        Table joinedTable = sql.join(userTable, purchaseTable, "USER_ID", "USER_ID");

        // Check the size of the content
        Assert.assertEquals(joinedTable.getRowCount(), 8);

        // Sort and check the first row that contains data from both tables.
        Table sortedTable = sql.orderByDesc(joinedTable, "AD_ID");
        Row firstRow = sortedTable.getRow(0);

        Assert.assertEquals("9", firstRow.getValue("AD_ID"));
        Assert.assertEquals("andre", firstRow.getValue("NAME"));
    }

    @Test
    public void testNoSuccessJoin() {
        Sql sql = new Sql();

        Table userTable = createTable(USER_FILE_PATH);
        Table purchaseTable = createTable(PURCHASE_FILE_PATH);

        Table joinedTable = sql.join(userTable, purchaseTable, "USER_ID", "TITLE");
        Assert.assertEquals(0, joinedTable.getRowCount());
    }

}
