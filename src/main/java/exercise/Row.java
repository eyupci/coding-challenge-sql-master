package exercise;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Row {

    private Map<String, String> data;

    public Row(String[] values, List<String> columns) {
        data = new HashMap<>();
        for (int i = 0; i < values.length; i++) {
            data.put(columns.get(i), values[i]);
        }
    }

    public String getValue(String column) {
        return data.get(column);
    }

    public Map<String, String> getData() {
        return data;
    }
}
