package exercise;

import com.opencsv.CSVReader;
import helper.RowFactory;
import helper.TableBuilder;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class Sql {

    public Table init(InputStream csvContent) throws Exception {
        CSVReader csvReader = new CSVReader(new InputStreamReader(csvContent));
        String[] line;

        List<String> headers = new ArrayList<>();
        List<String[]> rows = new ArrayList<>();


        int lineCount = 0;
        while ((line = csvReader.readNext()) != null) {
            if (lineCount == 0) {
                headers = Arrays.asList(line);
            } else {
                rows.add(line);
            }
            lineCount++;
        }

        TableBuilder builder = new TableBuilder().addColumns(headers);
        for (String[] rowArray : rows) {
            builder.addRow(RowFactory.create(rowArray, headers));
        }
        return builder.build();
    }

    public Table orderByDesc(Table table, String columnName) {
        Comparator<Row> compareByColumn = Comparator.comparing((Row item) -> item.getValue(columnName)).reversed();
        List<Row> rows = table.getRows();
        Collections.sort(rows, compareByColumn);

        Table orderedTable = new TableBuilder().addColumns(table.getColumns()).addRows(rows).build();
        return orderedTable;

    }

    public Table join(Table left, Table right, String joinColumnTableLeft, String joinColumnTableRight) {

        Set<String> columnsSet = new HashSet<>();
        columnsSet.addAll(left.getColumns());
        columnsSet.addAll(right.getColumns());

        List<String> columns = new ArrayList<>(columnsSet);


        TableBuilder tableBuilder = new TableBuilder().addColumns(columns);

        for (Row row : left.getRows()) {
            String value = row.getValue(joinColumnTableLeft);
            for (Row row2 : right.getRows()) {
                if (value.equals(row2.getValue(joinColumnTableRight))) {
                    Row mergedRow = RowFactory.merge(row, row2, columns);
                    tableBuilder.addRow(mergedRow);
                }
            }
        }
        return tableBuilder.build();
    }

}
