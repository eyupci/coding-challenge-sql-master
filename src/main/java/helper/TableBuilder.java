package helper;

import exercise.Row;
import exercise.Table;

import java.util.ArrayList;
import java.util.List;

public class TableBuilder {

    private List<String> columns;

    private List<Row> rows;

    public TableBuilder() {
        columns = new ArrayList<>();
        rows = new ArrayList<>();
    }

    public TableBuilder addColumns(List<String> columns) {
        for (String column : columns) {
            if (this.columns.contains(column)) {
                continue;
            }
            this.columns.add(column);
        }
        return this;
    }

    public TableBuilder addRows(List<Row> rows) {
        this.rows.addAll(rows);
        return this;
    }

    public TableBuilder addRow(Row row) {
        this.rows.add(row);
        return this;
    }

    public Table build() {
        Table table = new Table(this.columns, this.rows);
        return table;
    }

}
