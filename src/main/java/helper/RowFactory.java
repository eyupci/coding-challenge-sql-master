package helper;

import exercise.Row;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RowFactory {

    public static Row create(String[] values, List<String> headers) {
        return new Row(values, headers);
    }

    public static Row merge(Row row1, Row row2, List<String> columns) {

        HashMap<String, String> valuesStack = new HashMap<>();
        valuesStack.putAll(row1.getData());
        valuesStack.putAll(row2.getData());

        List<String> values = new ArrayList<>();
        for (String column : columns) {
            String value = valuesStack.get(column);
            if (value != null) {
                values.add(value);
            }

        }
        return new Row(values.toArray(String[]::new), columns);
    }
}
